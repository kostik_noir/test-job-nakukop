import { mountPoint } from './constants';
import { CartItem } from './types';

export const getItems = (state: any) => state[mountPoint].items;

export const isInCart = (state: any, itemId: string) =>
  state[mountPoint].items.filter((obj: CartItem) => obj.id === itemId).length > 0;
