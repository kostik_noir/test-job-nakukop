export type CartItem = {
  id: string;
  title: string;
  price: number;
  amount: number;
}
