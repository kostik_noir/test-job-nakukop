export const mountPoint = 'cart';
export const itemAdded = `${mountPoint}/itemAdded`;
export const itemRemoved = `${mountPoint}/itemRemoved`;
export const itemQuantityChanged = `${mountPoint}/itemQuantityChanged`;
export const restoreItems = `${mountPoint}/restoreItems`;
export const itemsRestored = `${mountPoint}/itemsRestored`;
