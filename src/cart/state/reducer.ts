import * as constants from './constants';
import { CartItem } from './types';
import { Action, ReduxStore } from '../../common/types';

const initialState = {
  items: []
};

export default (state = initialState, action: Action) => {
  switch (action.type) {
    case constants.itemAdded:
      return addItem(state, action);
    case constants.itemRemoved:
      return removeItem(state, action);
    case constants.itemQuantityChanged:
      return changeItemQuantity(state, action);
    case constants.itemsRestored:
      return restoreItems(state, action);
    default:
      return state;
  }
}

function addItem(state: ReduxStore, action: Action) {
  const { payload: { item } } = action;

  if (findItemIndex(state.items, item.id) !== -1) {
    return state;
  }

  return {
    ...state,
    items: [
      ...state.items,
      ...[{ ...item, amount: 1 }]
    ]
  }
}

function removeItem(state: ReduxStore, action: Action) {
  const { payload: { itemId } } = action;
  const index: number = findItemIndex(state.items, itemId);

  if (index === -1) {
    return state;
  }

  return {
    ...state,
    items: [
      ...state.items.slice(0, index),
      ...state.items.slice(index + 1)
    ]
  };
}

function changeItemQuantity(state: ReduxStore, action: Action) {
  const { payload: { itemId, quantity } } = action;
  if (quantity < 1) {
    return state;
  }

  const index: number = findItemIndex(state.items, itemId);

  if (index === -1) {
    return state;
  }

  const item = {
    ...state.items[index],
    amount: quantity
  };

  return {
    ...state,
    items: [
      ...state.items.slice(0, index),
      ...[item],
      ...state.items.slice(index + 1)
    ]
  };
}

function restoreItems(state: ReduxStore, action: Action) {
  const { payload: { items: rawItems } } = action;

  let items = rawItems;
  if (!Array.isArray(items)) {
    items = [];
  }

  return {
    ...state,
    items
  };
}

function findItemIndex(items: CartItem[], itemId: string): number {
  const count: number = items.length;
  for (let i: number = 0; i < count; i++) {
    if (items[i].id === itemId) {
      return i;
    }
  }
  return -1;
}
