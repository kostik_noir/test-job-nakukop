import * as constants from './constants';
import { CartItem } from './types';

export const addItem = (item: any) => ({
  type: constants.itemAdded,
  payload: { item }
});

export const removeItem = (itemId: string) => ({
  type: constants.itemRemoved,
  payload: { itemId }
});

export const itemQuantityChanged = (itemId: string, quantity: number) => ({
  type: constants.itemQuantityChanged,
  payload: { itemId, quantity }
});

export const restoreItems = () => ({
  type: constants.restoreItems
});

export const itemsRestored = (items: CartItem[]) => ({
  type: constants.itemsRestored,
  payload: { items }
});
