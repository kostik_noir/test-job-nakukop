import * as constants from '../constants';
import * as selectors from '../selectors';
import * as actionCreators from '../action-creators';
import { Action, ReduxNextFn, ReduxStore } from '../../../common/types';
import { CartItem } from '../types';

const localStorageKey = 'nakupop-cart';

export default (store: ReduxStore) => (next: ReduxNextFn) => (action: Action) => {
  next(action);

  switch (action.type) {
    case constants.itemAdded:
    case constants.itemRemoved:
    case constants.itemQuantityChanged:
      saveData(selectors.getItems(store.getState()));
      break;
    case constants.restoreItems:
      restoreItems(store);
      break;
  }
};

function saveData(items: CartItem[]) {
  try {
    localStorage.setItem(localStorageKey, JSON.stringify(items));
  } catch (e) {
  }
}

function restoreItems(store: ReduxStore) {
  try {
    const items = JSON.parse(localStorage.getItem(localStorageKey) || '[]');
    store.dispatch(actionCreators.itemsRestored(items));
  } catch (e) {
  }
}
