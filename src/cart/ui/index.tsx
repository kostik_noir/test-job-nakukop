import { useSelector, useDispatch } from 'react-redux';
import * as cartState from '../state';
import { CartItem } from '../state/types';
import Item from './item';
import styles from './styles.module.scss';

const Cart = () => {
  const dispatch = useDispatch();

  const items = useSelector(cartState.selectors.getItems);

  const changeAmount = (id: string, value: number) => {
    dispatch(cartState.actionCreators.itemQuantityChanged(id, value));
  };

  const removeItem = (id: string) => {
    dispatch(cartState.actionCreators.removeItem(id));
  };

  return (
    <div className={styles.root}>
      <div className={styles.table}>
        <div className={styles.header}>
          <div className={styles.cell}>Наименование товара</div>
          <div className={styles.cell}>Количество</div>
          <div className={styles.cell}>Цена</div>
          <div className={styles.cell}></div>
        </div>

        <>
          {
            items.map((item: CartItem) =>
              <Item data={item} onAmountChange={changeAmount} onRemove={removeItem} key={item.id}/>
            )
          }
        </>
      </div>
    </div>
  );
};

export default Cart;
