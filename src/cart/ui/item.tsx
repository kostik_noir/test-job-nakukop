import { ChangeEvent } from 'react';
import { CartItem } from '../state/types';
import styles from './styles.module.scss';

interface Props {
  data: CartItem;
  onAmountChange: (id: string, value: number) => void;
  onRemove: (id: string) => void;
}

const Item = ({ data, onAmountChange, onRemove }: Props) => {
  const { id, title, price, amount } = data;

  const onAmountFieldChange = (event: ChangeEvent<HTMLInputElement>) => {
    onAmountChange(id, parseInt(event.target.value));
  };

  const onDeleteBtnPressed = () => {
    onRemove(id);
  };

  return (
    <div className={styles.item}>
      <div className={styles.cell}>{title}</div>
      <div className={styles.cell}>
        <div className={styles.amountField}>
          <input className={styles.amountInput}
                 type="number" min="1" value={amount} onChange={onAmountFieldChange}/>
          <div className={styles.amountLabel}>шт</div>
        </div>
      </div>
      <div className={styles.cell}>{price.toFixed(2)} / шт</div>
      <div className={styles.cell}>
        <div className={styles.deleteBtn} onClick={onDeleteBtnPressed}>Удалить</div>
      </div>
    </div>
  );
};

export default Item;
