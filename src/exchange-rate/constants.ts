export const mountPoint = 'exchange-rate';
export const exchangeRateChanged = `${mountPoint}/exchangeRateChanged`;
export const startTrackingExchangeRate = `${mountPoint}/startTrackingExchangeRate`;
export const stopTrackingExchangeRate = `${mountPoint}/stopTrackingExchangeRate`;

export const minRate = 50;
export const maxRate = 80;
