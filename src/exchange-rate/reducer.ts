import { Action } from '../common/types';
import * as constants from './constants';
import { State } from './types';

const initialState = {
  value: 70
};

export default (state = initialState, action: Action) => {
  switch (action.type) {
    case constants.exchangeRateChanged:
      return onExchangeRateChanged(state, action);
    default:
      return state;
  }
}

function onExchangeRateChanged(state: State, action: Action) {
  const { payload: { value } } = action;
  if (value > constants.maxRate || value < constants.minRate) {
    return state;
  }

  return {
    ...state,
    value
  };
}
