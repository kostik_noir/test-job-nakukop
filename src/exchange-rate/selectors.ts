import { mountPoint } from './constants';
import { ReduxState } from '../common/types';

export const getExchangeRate = (state: ReduxState) => state[mountPoint].value;
