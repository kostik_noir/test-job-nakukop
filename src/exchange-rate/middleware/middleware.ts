import { Action } from '../../common/types';
import * as constants from '../constants';
import * as actionCreators from '../action-creators';
import { ReduxStore, ReduxNextFn } from '../../common/types';

interface State {
  handleAction: (action: Action) => void;
}

export default (store: ReduxStore) => {
  let state: State;

  stateIdle();

  return (next: ReduxNextFn) => (action: Action) => {
    next(action);

    state.handleAction(action);
  };

  function stateIdle() {
    state = {
      handleAction
    };

    function handleAction(action: Action) {
      if (action.type === constants.startTrackingExchangeRate) {
        stateActive();
      }
    }
  }

  function stateActive() {
    let timeoutId = NaN;

    state = {
      handleAction
    };

    startDelay();

    function handleAction(action: Action) {
      if (action.type === constants.stopTrackingExchangeRate) {
        clearInterval(timeoutId);
        stateIdle();
      }
    }

    function startDelay() {
      timeoutId = window.setTimeout(() => {
        updateValue();
        startDelay();
      }, 5000);
    }

    function updateValue() {
      const { minRate, maxRate } = constants;
      const value = Math.floor(minRate + Math.random() * (maxRate - minRate));
      store.dispatch(actionCreators.exchangeRateChanged(value));
    }
  }
};
