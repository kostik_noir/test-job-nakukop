import * as constants from './constants';

export const exchangeRateChanged = (value: number) => ({
  type: constants.exchangeRateChanged,
  payload: { value }
});

export const startTrackingExchangeRate = () => ({
  type: constants.startTrackingExchangeRate
});

export const stopTrackingExchangeRate = () => ({
  type: constants.stopTrackingExchangeRate
});
