import * as constants from '../constants';
import * as actionCreators from '../action-creators';
import { Action, ReduxStore, ReduxNextFn } from '../../../common/types';

interface State {
  handleAction: (action: Action) => void;
}

export default (store: ReduxStore) => {
  let state: State;

  stateWaitStartGoodsTracking();

  return (next: ReduxNextFn) => (action: Action) => {
    next(action);
    state.handleAction(action);
  };

  function stateWaitStartGoodsTracking() {
    state = {
      handleAction
    };

    function handleAction(action: Action) {
      if (action.type === constants.startGoodsTracking) {
        stateLoadData();
      }
    }
  }

  function stateLoadData() {
    let isCanceled = false;

    state = {
      handleAction
    };

    loadData()
      .then((data) => {
        if (isCanceled) {
          return;
        }
        store.dispatch(actionCreators.loadingCompleted(parseData(data)));
        stateDelay();
      })
      .catch(() => {
        if (isCanceled) {
          return;
        }
        stateDelay();
      });

    function handleAction(action: Action) {
      if (action.type === constants.stopGoodsTracking) {
        isCanceled = true;
        stateWaitStartGoodsTracking();
      }
    }

    async function loadData() {
      const products = await fetch(`/products.json?_${Date.now()}`).then(res => res.json());
      const names = await fetch(`/names.json?_${Date.now()}`).then(res => res.json());
      return { products, names };
    }
  }

  function stateDelay() {
    const timeoutId = setTimeout(() => {
      stateLoadData();
    }, 15000);

    state = {
      handleAction
    };

    function handleAction(action: Action) {
      if (action.type === constants.stopGoodsTracking) {
        clearInterval(timeoutId);
        stateWaitStartGoodsTracking();
      }
    }
  }
};

function parseData({ products, names }: { products: any, names: any }): any {
  const goods: any = {};

  const findItemDetails = (itemId: string) => {
    const record = products.Value.Goods.filter((obj: any) => `${obj.T}` === `${itemId}`)[0];
    if (typeof record === 'undefined') {
      return null;
    }

    return {
      price: record.C,
      amount: record.P
    };
  };

  const groups = Object.keys(names)
    .reduce((acc: any[], groupId) => {
      const groupData = names[groupId];
      const title = groupData.G;
      const itemIds = Object.keys(groupData.B);

      const validItemIds: string[] = [];

      itemIds.forEach((id: string) => {
        const details = findItemDetails(id);
        if (details === null) {
          return;
        }

        validItemIds.push(id);

        const { price, amount } = details;

        goods[id] = {
          id,
          title: groupData.B[id].N,
          price,
          amount
        };
      });

      if (validItemIds.length === 0) {
        return acc;
      }

      acc.push({
        id: groupId,
        title,
        items: validItemIds
      });

      return acc;
    }, []);

  return { groups, goods };
}
