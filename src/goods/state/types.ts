export type State = any

export type Group = {
  id?: string;
  title: string;
  items: [];
}
