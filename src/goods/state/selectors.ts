import { mountPoint } from './constants';
import { State } from './types';

export const getGroups = (state: State) => state[mountPoint].groups;

export const getGoodById = (state: State, id: string) => state[mountPoint].goods[id];
