import * as constants from './constants';

export const startGoodsTracking = () => ({
  type: constants.startGoodsTracking
});

export const stopGoodsTracking = () => ({
  type: constants.stopGoodsTracking
});

export const loadingCompleted = ({ groups, goods }: { groups: any, goods: any }) => ({
  type: constants.loadingCompleted,
  payload: { groups, goods }
});
