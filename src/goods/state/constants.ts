export const mountPoint = 'goods';
export const startGoodsTracking = `${mountPoint}/startGoodsTracking`;
export const stopGoodsTracking = `${mountPoint}/stopGoodsTracking`;
export const loadingCompleted = `${mountPoint}/loadingCompleted`;
