import { Action } from '../../common/types';
import * as constants from './constants';
import { State } from './types';

const initialState = {
  groups: [],
  goods: {}
};

export default (state: State = initialState, action: Action) => {
  switch (action.type) {
    case constants.loadingCompleted:
      return onLoadingCompleted(state, action);
    default:
      return state;
  }
}

function onLoadingCompleted(state: State, action: Action) {
  const { payload: { groups, goods } } = action;
  return {
    ...state,
    groups,
    goods
  };
}
