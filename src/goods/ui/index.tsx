import { useEffect } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import * as goodsState from '../state';
import * as exchangeRateState from '../../exchange-rate';
import Group from './group';
import { Group as GroupType } from '../state/types';

const Goods = () => {
  const dispatch = useDispatch();

  const groups = useSelector(goodsState.selectors.getGroups, shallowEqual);

  useEffect(() => {
    dispatch(goodsState.actionCreators.startGoodsTracking());
    dispatch(exchangeRateState.actionCreators.startTrackingExchangeRate());

    return () => {
      dispatch(goodsState.actionCreators.stopGoodsTracking());
      dispatch(exchangeRateState.actionCreators.stopTrackingExchangeRate());
    };
  }, [dispatch]);

  return (
    <>
      {
        groups.map((group: GroupType) => <Group title={group.title} items={group.items} key={group.id}/>)
      }
    </>
  );
};

export default Goods;
