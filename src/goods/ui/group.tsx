import Item from './item';
import { Group as GroupType } from '../state/types';
import styles from './styles.module.scss';

const Group = ({ title, items }: GroupType) => (
  <div className={styles.group}>
    <div className={styles.groupTitle}>{title}</div>
    <div className={styles.groupItems}>
      {
        items.map((id: string) => <Item id={id} key={id}/>)
      }
    </div>
  </div>
);

export default Group;
