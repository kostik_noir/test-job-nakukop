import { useRef, useEffect, useState } from 'react';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import * as goodsState from '../state';
import * as exchangeRateState from '../../exchange-rate';
import * as cartState from '../../cart/state';
import styles from './styles.module.scss';

interface Props {
  id: string
}

type Ref = {
  current: any;
}

const PRICE_INCREASED = 1;
const PRICE_DECREASED = 2;
const PRICE_NOT_CHANGED = 3;

const Item = ({ id }: Props) => {
  const dispatch = useDispatch();

  const data = useSelector(state => goodsState.selectors.getGoodById(state, id), shallowEqual);
  const isInCart = useSelector(state => cartState.selectors.isInCart(state, id));

  const exchangeRate = useSelector(exchangeRateState.selectors.getExchangeRate);
  const [priceChangeDirection, setPriceChangeDirection] = useState(PRICE_NOT_CHANGED);
  const prevResultPrice: Ref = useRef();

  const { title, amount, price } = data;

  const resultPrice = Math.floor(price * exchangeRate * 100) / 100;

  useEffect(() => {
    const { current: prevValue }: { current: number | undefined } = prevResultPrice;

    if (typeof prevValue === 'undefined') {
      setPriceChangeDirection(PRICE_NOT_CHANGED);
      return;
    }

    switch (true) {
      case resultPrice === prevValue:
        setPriceChangeDirection(PRICE_NOT_CHANGED);
        break;
      case resultPrice > prevValue:
        setPriceChangeDirection(PRICE_INCREASED);
        break;
      case resultPrice < prevValue:
        setPriceChangeDirection(PRICE_DECREASED);
        break;
    }

    prevResultPrice.current = resultPrice;
  }, [resultPrice]);

  let priceCellClassNames: string[] = [styles.itemCell];
  switch (priceChangeDirection) {
    case PRICE_INCREASED:
      priceCellClassNames.push(styles.priceIncreased);
      break;
    case PRICE_DECREASED:
      priceCellClassNames.push(styles.priceDecreased);
      break;
  }
  const priceCellClassName: string = priceCellClassNames.join(' ');

  let buyBtnClassNames = [styles.buyBtn];
  if (isInCart) {
    buyBtnClassNames.push(styles.disabled);
  }
  const btnClassName = buyBtnClassNames.join(' ');

  const onBuyBtnPressed = () => {
    const data = {
      id,
      title,
      price: resultPrice
    };
    dispatch(cartState.actionCreators.addItem(data));
  };

  return (
    <div className={styles.item}>
      <div className={styles.itemCell}>{title}</div>
      <div className={styles.itemCell}>{amount}</div>
      <div className={priceCellClassName}>{resultPrice.toFixed(2)}</div>
      <div className={styles.itemCell}>
        <div className={btnClassName} onClick={onBuyBtnPressed}>купить</div>
      </div>
    </div>
  );
};

export default Item;
