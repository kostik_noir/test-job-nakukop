import Layout from '../common/layout'
import CartUi from '../cart/ui';

const Cart = () => (
  <Layout title="Cart">
    <CartUi/>
  </Layout>
);

export default Cart;
