import { AppProps } from 'next/app';
import { Provider } from 'react-redux';
import createStore from '../store';

// XXX distribute appropriate `store` across different pages if using SSG or setup a SPA
const App = ({ Component, pageProps }: AppProps) => (
  <Provider store={createStore()}>
    <Component {...pageProps} />
  </Provider>
);

export default App;
