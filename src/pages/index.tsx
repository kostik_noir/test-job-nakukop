import Layout from '../common/layout';
import Goods from '../goods/ui';

const IndexPage = () => (
  <Layout title="Home">
    <Goods/>
  </Layout>
);

export default IndexPage
