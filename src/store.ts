import { createStore, combineReducers, applyMiddleware } from 'redux';
import * as goods from './goods/state';
import * as exchangeRate from './exchange-rate';
import * as cart from './cart/state';

export default () => {
  const reducers = {
    [goods.mountPoint]: goods.reducer,
    [exchangeRate.mountPoint]: exchangeRate.reducer,
    [cart.mountPoint]: cart.reducer
  };

  const middleware = [
    ...goods.middleware,
    ...exchangeRate.middleware,
    ...cart.middleware
  ];

  const store: any = createStore(combineReducers(reducers), applyMiddleware(...middleware));
  store.dispatch(cart.actionCreators.restoreItems());
  return store;
}
