export type Action = {
  type: string;
  payload?: any;
}

export type ReduxNextFn = (action: Action) => Action;
export type ReduxStore = any;
export type ReduxState = any;
